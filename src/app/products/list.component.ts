﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { UploadService, ProductService, CategoryService, AccountService, AlertService } from '../_services';
import { Product, Category, User } from '../_models';

import { Subscription } from 'rxjs';
import * as _ from 'lodash';

@Component({ templateUrl: 'list.component.html' })
export class ProductsListComponent implements OnInit {
  categories!: Category[];
  products!: Product[];
  user: User;
  public selectedItem: any;

  progress: number;
  message: string;
  onUploadFinished: any;
  imageBytes: any;

  uploadProgressSub: Subscription;
  uploadMessageSub: Subscription;
  uploadCompleteSub: Subscription;
  uploadImageBytesSub: Subscription;

  constructor(
    private productService: ProductService,
    private accountService: AccountService,
    private alertService: AlertService,
    private categoryService: CategoryService,
    private uploadService: UploadService) {
    this.user = this.accountService.userValue;
  }

  ngOnInit() {
    //populate the category dropdown
    this.categoryService.getAllByUserId(this.user.id)
      .pipe(first())
      .subscribe(categories => {
        this.categories = categories;
      });

    //subscribe to the upload service observables
    this.uploadProgressSub = this.uploadService.onProgress()
      .subscribe(data => {
        this.progress = data;
      });

    this.uploadMessageSub = this.uploadService.onMessage()
      .subscribe(data => {
        this.message = data;
      });

    this.uploadCompleteSub = this.uploadService.onUploadComplete()
      .subscribe(data => {
        this.onUploadFinished = data.body;
      });

    this.uploadImageBytesSub = this.uploadService.onBytes()
      .subscribe(bytes => {
        this.imageBytes = bytes;
      });
  }

  onCategoryChange() {
    this.productService.getAllByCategoryId(this.selectedItem)
      .pipe(first())
      .subscribe({
        next: data => {
          this.products = data;
          $(function () {
            $("#dtProducts").DataTable(
              {
                "order": [[1, "asc"]]
              }
            );
          });
        },
        complete: () => {
          this.alertService.loading(false)
        }
      });
  }

  deleteProduct(id: number) {
    this.alertService.loading(true);

    const product = this.products.find(x => x.id === id);
    if (!product) return;
    this.productService.delete(id)
      .pipe(first())
      .subscribe(() => {
        this.products = this.products.filter(x => x.id !== id);
        this.alertService.success('Product deleted');
        this.alertService.loading(false);
      });
  }

  public uploadFile = (files: string | any[], event: { target: { files: string | any[]; }; }) => {
    if (files.length === 0) {
      return;
    }

    let af = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel']
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      if (!_.includes(af, file.type)) {
        this.alertService.error('Only EXCEL Docs Allowed!');
      }
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.uploadService.uploadProductsExcel(formData, this.selectedItem);

    this.alertService.loading(true);
    this.uploadService.bytes.subscribe({
      next: data => {
        if (data) {
          this.alertService.success('Products uploaded to selected category');
        }
        else {
          this.alertService.error('Unable to upload products!');
        }
      },
      complete: () => {
        this.alertService.loading(false);
      }
    })
  }

  public downloadFile = () => {
    this.uploadService.downloadProductsExcel(this.selectedItem);

    this.alertService.loading(true);
    this.uploadService.bytes.subscribe({
      next: data => {
        if (data) {
          this.alertService.success('Products downloaded from selected category');
        }
        else {
          this.alertService.error('Unable to download products!');
        }
      },
      complete: () => {
        this.alertService.loading(false);
      }
    })
  }
}