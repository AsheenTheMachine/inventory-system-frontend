﻿import { Component, OnInit, Type } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';


import { UploadService, CategoryService, ProductService, AlertService, AccountService } from '../_services';
import { Product, Category, User } from '../_models';
import { Subscription } from 'rxjs';

@Component({ templateUrl: 'add-edit.component.html' })
export class AddEditComponent implements OnInit {
    form!: FormGroup;
    id!: number;
    isAddMode!: boolean;
    loading = false;
    submitted = false;
    product: Product;
    user: User;
    categories!: Category[];
    fileData: any;

    progress: number;
    message: string;
    onUploadFinished: any;
    imageBytes: any;

    uploadProgressSub: Subscription;
    uploadMessageSub: Subscription;
    uploadCompleteSub: Subscription;
    uploadImageBytesSub: Subscription;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private productService: ProductService,
        private alertService: AlertService,
        private accountService: AccountService,
        private categoryService: CategoryService,
        private uploadService: UploadService
    ) {
        this.user = this.accountService.userValue;
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.form = this.formBuilder.group({
            code:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(6),
                        Validators.maxLength(6)
                    ]
                ],
            name:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50)
                    ]
                ],
            description:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50)
                    ]
                ],
            price:
                [
                    '',
                    [
                        Validators.required
                    ]
                ],
            category:
                [
                    '',
                    [
                        Validators.required,
                        Validators.min(1)
                    ]
                ],
            isActive: [''],
        });

        //populate the category ddl
        this.alertService.loading(true);
        this.categoryService.getAllByUserId(this.user.id)
            .pipe(first())
            .subscribe({
                next: data => {
                    this.categories = data;
                    this.f.category.setValue(0);
                },
                complete: () => {
                    this.alertService.loading(false)
                }
            });


        //check if not in add mode, then populate the form
        if (!this.isAddMode) {
            this.alertService.loading(true);
            this.productService.getById(this.id)
                .pipe(first())
                .subscribe(x => {
                    this.form.patchValue(x);
                    this.f.category.setValue(x.categoryId);
                    this.imageBytes = x.image;
                    this.alertService.loading(false)
                });
        }

        //subscribe to the upload service observables
        this.uploadProgressSub = this.uploadService.onProgress()
            .subscribe(data => {
                this.progress = data;
            });

        this.uploadMessageSub = this.uploadService.onMessage()
            .subscribe(data => {
                this.message = data;
            });

        this.uploadCompleteSub = this.uploadService.onUploadComplete()
            .subscribe(data => {
                this.onUploadFinished = data.body;
            });

        this.uploadImageBytesSub = this.uploadService.onBytes()
            .subscribe(bytes => {
                this.imageBytes = bytes;
            });

    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.alertService.loading(true);

        if (this.isAddMode) {
            this.create();
        } else {
            this.update();
        }
    }

    private create() {
        //populate the product model from form values
        this.product = this.form.value;
        this.product.categoryId = this.f.category.value;
        this.product.image = this.imageBytes;

        //save product
        this.productService.create(this.product)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Product added!', { keepAfterRouteChange: true });
                    this.router.navigate(['../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                    this.alertService.loading(false);
                },
                complete: () => {
                    this.alertService.loading(false)
                }
            })
            .add(() => this.loading = false);
    }

    private update() {
        this.product = this.form.value;
        this.product.categoryId = this.f.category.value;
        this.product.image = this.imageBytes;

        this.productService.update(this.id, this.product)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Product updated!', { keepAfterRouteChange: true });
                    this.router.navigate(['../../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                    this.alertService.loading(false);
                },
                complete: () => {
                    this.alertService.loading(false)
                }
            })
            .add(() => this.loading = false);
    }

    public uploadFile = (files: string | any[]) => {
        if (files.length === 0) {
            return;
        }
        let fileToUpload = <File>files[0];
        const formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);

        this.uploadService.uploadProductImage(formData);
    }
}