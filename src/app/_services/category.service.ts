import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Category } from '../_models';

const baseUrl = `${environment.apiUrl}/category`;

@Injectable({ providedIn: 'root' })
export class CategoryService {

    constructor( private http: HttpClient) {}

    //add new category
    create(params: any) {
        return this.http.post(`${baseUrl}/new`, params);
    }

    //get a list of categories by userId
    getAllByUserId(userId: number) {
        return this.http.get<Category[]>(`${baseUrl}/list/${userId}`);
    }

    //get single category by id
    getById(id: number, userId: number) {
        return this.http.get<Category>(`${baseUrl}/${id}/${userId}`);
    }

    //update category
    update(id: number, params: any) {
        return this.http.put(`${baseUrl}/edit/${id}`, params)
            .pipe(map(x => {
                return x;
            }));
    }

    //delete catergory
    delete(id: number) {
        return this.http.delete(`${baseUrl}/remove/${id}`)
            .pipe(map(x => {
                return x;
            }));
    }
}