import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { saveAs } from 'file-saver';
const baseUrl = `${environment.apiUrl}`;

@Injectable({ providedIn: 'root' })
export class UploadService {

  public progress = new Subject<number>();
  public message = new Subject<string>();
  public onUploadFinished = new Subject<HttpResponse<Object>>();
  public bytes = new Subject<string>();

  constructor(private http: HttpClient) { }

  onProgress(): Observable<number> {
    return this.progress.asObservable();
  }

  onMessage(): Observable<string> {
    return this.message.asObservable();
  }

  onUploadComplete(): Observable<HttpResponse<Object>> {
    return this.onUploadFinished.asObservable();
  }

  onBytes(): Observable<string> {
    return this.bytes.asObservable();
  }

  //upload product image to API. at the same time we are going to track the progress and update the UI
  uploadProductImage(formData: any) {
    this.http.post(`${baseUrl}/product/image/upload`, formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress.next(Math.round(100 * event.loaded / event.total));
        else if (event.type === HttpEventType.Response) {
          this.message.next('Upload success.');
          //this.onUploadFinished.next(event.body);

          let respJSON = JSON.parse(JSON.stringify(event.body));
          this.bytes.next(respJSON.data);

          this.onUploadFinished.next(event);
        }
      });
  }

  //upload excel sheet of products for a specific category to API
  uploadProductsExcel(formData: any, categoryId: Number) {
    this.http.post(`${baseUrl}/product/excel/upload?catId=${categoryId}`, formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress.next(Math.round(100 * event.loaded / event.total));
        else if (event.type === HttpEventType.Response) {
          this.message.next('Upload success.');

          let respJSON = JSON.parse(JSON.stringify(event.body));
          this.bytes.next(respJSON.data);

          this.onUploadFinished.next(event);
        }
      });
  }

  //download excel sheet of products for a specific category from API
  downloadProductsExcel(categoryId: Number) {
    this.http.get(`${baseUrl}/product/excel/download?catId=${categoryId}`, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress.next(Math.round(100 * event.loaded / event.total));
        else if (event.type === HttpEventType.Response) {
          this.message.next('Download complete.');

          let respJSON = JSON.parse(JSON.stringify(event.body));
          this.bytes.next(respJSON.data);

          this.onUploadFinished.next(event);

          const byteCharacters = atob(respJSON.data);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);

          const blob = new Blob([byteArray],
            { type: 'application/vnd.ms-excel' });
          const file = new File([blob], respJSON.fileName + '.xlsx',
            { type: 'application/vnd.ms-excel' });

          saveAs.saveAs(file);
        }
      },
        err => {
          console.log(err);
          this.onUploadFinished.next(err);
        });
  }
}