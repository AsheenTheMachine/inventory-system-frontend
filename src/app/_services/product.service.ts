import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Product } from '../_models';

const baseUrl = `${environment.apiUrl}/product`;

@Injectable({ providedIn: 'root' })
export class ProductService {

    constructor( private http: HttpClient) {}

    //add new product
    create(params: any) {
        return this.http.post(`${baseUrl}/new`, params);
    }

    //get single product by id
    getById(id: number) {
        return this.http.get<Product>(`${baseUrl}/edit/${id}`);
    }

    //get list of products by categoryId
    getAllByCategoryId(cagtegoryId: number) {
        return this.http.get<Product[]>(`${baseUrl}/list/${cagtegoryId}`);
    }

    //update product
    update(id: number, params: any) {
        return this.http.put(`${baseUrl}/edit/${id}`, params)
            .pipe(map(x => {
                return x;
            }));
    }

    //delete product
    delete(id: number) {
        return this.http.delete(`${baseUrl}/remove/${id}`)
            .pipe(map(x => {
                return x;
            }));
    }
}