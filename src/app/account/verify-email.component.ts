import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { AccountService } from '../_services';

@Component({ templateUrl: 'verify-email.component.html' })
export class VerifyEmailComponent implements OnInit {
    
    isValid = false;
    message = "";

    constructor(
        private route: ActivatedRoute,
        private accountService: AccountService
    ) { }

    ngOnInit() {
        const token = this.route.snapshot.queryParams['token'] || '';

        this.accountService.verifyEmail(token)
        .pipe(first())
        .subscribe({
            next: () => {
                //show success message
                this.isValid = true;
                this.message = "Account Verified!";
            },
            error: error => {
                //show error message
                this.isValid = false;
                this.message = error;
            }
        });   
    }
}