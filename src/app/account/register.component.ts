import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService, AlertService } from '../_services';
import { MustMatch } from '../_helpers/';

@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService
    ) { }

    ngOnInit() {

        const formOptions: AbstractControlOptions = { validators: MustMatch('password', 'confirmPassword') };
        
        this.form = this.formBuilder.group({
            name: [
                '',
                [
                    Validators.required, 
                    Validators.minLength(3), 
                    Validators.maxLength(50)
                ]
            ],
            surname: [
                '',
                [
                    Validators.required, 
                    Validators.minLength(3), 
                    Validators.maxLength(50)
                ]
            ],
            email: [
                '', 
                [
                    Validators.required, 
                    Validators.email, 
                    Validators.minLength(6), 
                    Validators.maxLength(100)
                ]
            ],
            password: [
                '', 
                [
                    Validators.required, 
                    Validators.minLength(6)
                ]
            ],
            confirmPassword: [
                '', 
                [
                    Validators.required, 
                    Validators.minLength(6)
                ]
            ]
        }, formOptions);
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
            this.loading = true;
            this.accountService.register(this.form.value)
                .pipe(first())
                .subscribe({
                    next: () => {
                        this.alertService.success('Registration successful', { keepAfterRouteChange: true });
                        this.router.navigate(['../login'], { relativeTo: this.route });
                    },
                    error: error => {
                        this.alertService.error(error);
                        this.loading = false;
                    }
                });
    }
}