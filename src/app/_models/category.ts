export class Category {
    id: number;
    code: string;
    name: string;
    isActive: boolean;
    userId: number;
}