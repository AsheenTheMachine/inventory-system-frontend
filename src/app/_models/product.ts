export class Product {
    id: number;
    code: string;
    name: string;
    description: string;
    categoryId: number;
    price: number;
    image: Blob;
}