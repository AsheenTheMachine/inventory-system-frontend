﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { CategoryService, AccountService, AlertService } from '../_services';
import { Category, User } from '../_models';

@Component({ templateUrl: 'list.component.html' })
export class CategoriesListComponent implements OnInit {
  categories!: Category[];
  user: User;

  constructor(
    private categoryService: CategoryService,
    private accountService: AccountService,
    private alertService: AlertService) {
    this.user = this.accountService.userValue;
  }

  ngOnInit() {
    this.alertService.loading(true);

    this.categoryService.getAllByUserId(this.user.id)
      .pipe(first())
      .subscribe({
        next: data => {
          this.categories = data;
          $(function () {
            $("#dtCategories").DataTable(
              {
                "order": [[1, "asc"]]
              }
            );
          });
        },
        complete: () => {
            this.alertService.loading(false)
        }
    });
  }

  deleteCategory(id: number) {
    this.alertService.loading(true);

    const category = this.categories.find(x => x.id === id);
    if (!category) return;
    this.categoryService.delete(id)
      .pipe(first())
      .subscribe(() => {
        this.categories = this.categories.filter(x => x.id !== id);
        this.alertService.success('Category deleted');
        this.alertService.loading(false);
      });
  }
}