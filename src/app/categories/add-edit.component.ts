﻿import { Component, OnInit, Type } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { CategoryService, AlertService, AccountService } from '../_services';
import { Category, User } from '../_models';

@Component({ templateUrl: 'add-edit.component.html' })
export class AddEditComponent implements OnInit {
    form!: FormGroup;
    id!: number;
    isAddMode!: boolean;
    loading = false;
    submitted = false;
    category: Category;
    user: User;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private categoryService: CategoryService,
        private alertService: AlertService,
        private accountService: AccountService
    ) {
        this.user = this.accountService.userValue;
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.form = this.formBuilder.group({
            code:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(6),
                        Validators.maxLength(6),
                        Validators.pattern("([A-Za-z]{3}[0-9]{3})")
                    ]
                ],
            name:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50)
                    ]
                ],
            isActive: [''],
        });

        if (!this.isAddMode) {
            this.alertService.loading(true);

            this.categoryService.getById(this.id, this.user.id)
                .pipe(first())
                .subscribe({
                    next: data => {
                        // get return url from query parameters or default to home page
                        this.form.patchValue(data)
                    },
                    complete: () => {
                        this.alertService.loading(false)
                    }
                });
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.alertService.loading(true);

        if (this.isAddMode) {
            this.create();
        } else {
            this.update();
        }
    }

    private create() {
        //populate the category model from form values, so we can add the userId
        this.category = this.form.value;
        this.category.userId = this.user.id;

        this.categoryService.create(this.category)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Category added!', { keepAfterRouteChange: true });
                    this.router.navigate(['../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                    this.alertService.loading(false);
                },
                complete: () => {
                    this.alertService.loading(false);
                }
            })
            .add(() => this.loading = false);
    }

    private update() {
        this.categoryService.update(this.id, this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Category updated!', { keepAfterRouteChange: true });
                    this.router.navigate(['../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                    this.alertService.loading(false);
                },
                complete: () => {
                    this.alertService.loading(false);
                }
            })
            .add(() => this.loading = false);
    }
}