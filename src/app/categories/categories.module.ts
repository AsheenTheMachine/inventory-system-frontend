import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { LayoutComponent } from './layout.component';
import { AddEditComponent } from './add-edit.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        CategoriesRoutingModule
    ],
    declarations: [
        LayoutComponent,
        AddEditComponent
    ]   
})
export class CategoriesModule { }