# README #

# About
Frontend application for managing stock items in an inventory system

# Features

1. Angular 10
2. Bootstrap UI
3. Connects to .Net Core 5.0 API
4. JWT Authentication
5. Reactive Forms
6. User registration and login
7. Email verification
8. Create categories
9. Create products
10. Upload product image with progress bar feature
11. Import products from Excel
12. Export products to Excel
13. Angular DataTables with sorting, paging and searching


# How to run the project

Before executing the following steps. Make sure you have the Database and .Net Core API running. API and database can be found here > https://bitbucket.org/AsheenTheMachine/inventory-system-backend/src/master/

1. Clone te project to your local machine
2. Open the project in Visual Studio Code
3. Open a new Terminal
4. Type `npm install` to install all node module dependencies
4. Type `ng serve` to host the application
5. After the project has compiled, open your browser and browse `http://localhost:4200/`

Note: Your API must be running on the following Url: `http://localhost:4000` To Change your API Url, update the apiUrl in `environments/environments.ts`


